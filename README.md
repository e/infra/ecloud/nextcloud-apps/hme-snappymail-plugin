# Hide my email plugin for snappymail


This plugin integrates hide-my-email app from nextcloud into snappymail. It uses nextcloud plugin of snappymail to interact with nextcloud DB to obtain aliases. 


## How to install

- Download the zip of this repo
- Move it to `nc/data/appdata_snappymail/_data_/_default_/plugins`
- unzip it
- Make sure that dir name is `hide-my-email` just as the classname in index.php
- Set ownership to www:data:www-data

```bash
chown -R www-data:www-data hide-my-email
```
- Set permissions to 700 to `hide-my-email` dir
```bash
chmod 700 hide-my-email
```
- Go to snappymail admin panel and enable the plugin


**Make sure to bump app version after new changes**

<?php


class HideMyEmailPlugin extends \RainLoop\Plugins\AbstractPlugin
{
	const
		NAME     = 'Hide my email',
		AUTHOR   = 'Murena',
		URL      = 'https://murena.io/settings/user/hide-my-email',
		VERSION  = '1.2.0',
		RELEASE  = '2022-11-29',
		REQUIRED = '2.21.0',
		CATEGORY = 'General',
		LICENSE  = 'MIT',
		DESCRIPTION = 'Plugin to integrate HME app into snappymail';

	public function Init() : void
	{

		$this->UseLangs(true); // start use langs folder

		// User Settings tab
		$this->addCss('css/style.css');
		$this->addJs('js/HMEUserSection.js'); // add js file
		$this->addTemplate('templates/HMESectionTab.html');
		$this->addJsonHook('JsonGetHMEAliases', 'GetHMEAliases');

	}

	public function Supported() : string
	{
		return static::IsIntegrated() ? '' : 'Nextcloud not found to use this plugin';
	}

	public static function IsIntegrated()
	{
		return \class_exists('OC') && isset(\OC::$server);
	}

	public static function IsLoggedIn()
	{
		return static::IsIntegrated() && \OC::$server->getUserSession()->isLoggedIn();
	}


	public function GetHMEAliases()
	{
		$aliases = [];
		try
		{
			$sUser =  \OC::$server->getUserSession()->getUser()->getUID();
			$config = \OC::$server->getConfig();
			$aliases = $config->getUserValue($sUser, 'hide-my-email', 'email-aliases', []);
		}
		catch (\Exception $oException)
		{
			if ($this->oLogger) {
				$this->oLogger->WriteException($oException);
			}
		}

		return $this->jsonResponse(__FUNCTION__, array(
			'aliases' => $aliases
		));
	}
}

((rl) => {
	if (rl) {
	  class HMESettings {
		constructor() {
		  this.aliases = ko.observableArray();
		}
		copyMail(index, data) {
		  navigator.clipboard.writeText(data[0]);
		  var elementid = "aliasbtn" + index;
		  document.getElementById(elementid).innerHTML = rl.i18n(
			"HIDE_MY_EMAIL/COPIED"
		  );
		  setTimeout(function () {
			document.getElementById(elementid).innerHTML =
			  rl.i18n("HIDE_MY_EMAIL/COPY");
		  }, 1000);
		}
		onBuild() {
		  rl.pluginRemoteRequest((iError, oData) => {
			if (!iError) {
			  this.aliases.push(JSON.parse(oData.Result.aliases));
			}
		  }, "JsonGetHMEAliases");
		}
	  }
	  rl.addSettingsViewModel(
		HMESettings,
		"HMESectionTab",
		"HIDE_MY_EMAIL/TAB_NAME",
		"hide-my-email"
	  );

	  addEventListener("rl-view-model.create", (e) => {
		if ("SystemDropDown" === e.detail.viewModelTemplateID) {
		  document
			.getElementById("SystemDropDown")
			.content.querySelector("menu .dividerbar + li + li")
			.before(
			  Element.fromHTML(`<li role="presentation">
						  <a href="#/settings/hide-my-email" tabindex="-1" data-icon="✉" data-i18n="HIDE_MY_EMAIL/TAB_NAME"></a>
					  </li>`)
			);
		}
	  });
	}
  })(window.rl);
